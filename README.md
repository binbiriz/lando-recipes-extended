# Lando Recipes Extended

This repository contains quick installation bash scripts to build docker based Lando development environments.

This is the continuation of 2 snippets that has already been released a few months ago.

1. [Lando ile hızlı Drupal 8 Kurulumu (klasik)](https://gitlab.com/binbiriz/lando-recipes-extended#lando-ile-h%C4%B1zl%C4%B1-drupal-8-kurulumu-klasik)
2. [Lando ile hızlı Drupal 8 Kurulumu (composer tabanlı)](https://gitlab.com/binbiriz/lando-recipes-extended#lando-ile-h%C4%B1zl%C4%B1-drupal-8-kurulumu-composer-tabanl%C4%B1)

Feel free 2 contribute!

## Lando ile hızlı Drupal 8 Kurulumu (klasik)

>Bu '*snippet*'
>
> * Ubuntu 18.04 (Bionic Beaver) Linux dağıtımı
> * Lando v3.0.0-rc.1
>
>için hazırlanmıştır.

Kullanabilmeniz için makinanızda:

1. docker-ce Kurulum için [Adım 1](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce) ve [Adım 2](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)
2. lando [Kurulum için](https://docs.devwithlando.io/installation/linux.html)
3. wget `sudo apt install wget`
4. unzip `sudo apt install unzip`
5. git `sudo apt install git`

önceden kurulu olmalıdır.

## Kullanımı

Bunları kurduktan sonra kurulumu yapacak olan script'i aşağıdaki komut ile (terminalde çalıştırılacak) makinanıza indiriniz:

> *İpucu*: Aşağıdaki komut aslında tek satırdır. Uzun olduğundan sondaki \ işareti ile satırlara bölünmüştür.

```bash
mkdir -p /home/$USER/scripts && \
wget -O /home/$USER/scripts/lando_d8_classic_install.sh \
  https://gitlab.com/binbiriz/lando-recipes-extended/raw/master/lando_d8_classic_install.sh?inline=false
```

Bu komut `/home/$USER/scripts` dizinini oluşturarak oraya `lando_d8_classic_install.sh` adında bir dosya koyar.

Daha sonra terminalde aşağıdaki komutu çalıştırarak kurulumu yapabilirsiniz.

```sh
bash /home/$USER/scripts/lando_d8_classic_install.sh "test-lando-script"
```

>`test-lando-script` yerine istediğiniz bir isim yazabilirsiniz. Lütfen bunu yazarken çift tırnak için alınız ve boşluk (white space) kullanmayınız.

Bu betik `/home/$USER/` dizininize `works/lando` şeklinde bir dizin açar ve dolayısıyla Drupal kurulumlarınızı `/home/$USER/works/lando` dizininde bulabilirsiniz.

## Kurulum sonrası

Drupal kurulumunuzda admin kullanıcı ve parola adı `admin` ve `admin` olarak düzenlenmiştir.

## Notlar

>İnternet hızınıza bağlı olarak maksimum 4-5 dakika sonra kurulu bir Drupal 8 ile başbaşa olacaksınız.
>
>Makinanızda Lando'yu ilk kez çalıştırıyorsanız, `docker` imajlarını ilk defa indireceğinden süre biraz daha uzayabilir. Ancak ikinci ve sonrasındaki çalıştırmalarda imaj indirilmeyeceğinden çok hızlı kurulum olacaktır.
>
>Bu script'te [composer tabanlı versiyondan](https://gitlab.com/snippets/1795406) farklı olarak bir kez `sudo` parola sormaktadır.
>
>Dikkat! Binbiriz'in iş akışlarında bütünlüğü korumak için bazı dizinlerin yerleri değiştirilmektedir. Örneğin `config` dizini `files` dizininden alınarak `drupal` dizininin içine taşınmaktadır.

## Lando ile hızlı Drupal 8 Kurulumu (composer tabanlı)

>Bu '*snippet*'
>
> * Ubuntu 18.04 (Bionic Beaver) Linux dağıtımı
> * Lando v3.0.0-rc.1
>
>için hazırlanmıştır.

Kullanabilmeniz için makinanızda:

1. docker-ce Kurulum için [Adım 1](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce) ve [Adım 2](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)
2. lando [Kurulum için](https://docs.devwithlando.io/installation/linux.html)
3. composer `sudo apt install composer`
4. git `sudo apt install git`

önceden kurulu olmalıdır.

## Kullanımı

Bunları kurduktan sonra kurulumu yapacak olan script'i aşağıdaki komut ile (terminalde çalıştırılacak) makinanıza indiriniz:

> *İpucu*: Aşağıdaki komut aslında tek satırdır. Uzun olduğundan sondaki \ işareti ile satırlara bölünmüştür.

```bash
mkdir -p /home/$USER/scripts && \
wget -O /home/$USER/scripts/lando_d8_composer_install.sh \
  https://gitlab.com/binbiriz/lando-recipes-extended/raw/master/lando_d8_composer_install.sh?inline=false
```

Bu komut `/home/$USER/scripts` dizinini oluşturarak oraya `lando_d8_composer_install.sh` adında bir dosya koyar.

Daha sonra terminalde aşağıdaki komutu çalıştırarak kurulumu yapabilirsiniz.

```sh
bash /home/$USER/scripts/lando_d8_composer_install.sh "test-lando-script"
```

>`test-lando-script` yerine istediğiniz bir isim yazabilirsiniz. Lütfen bunu yazarken çift tırnak için alınız ve boşluk (white space) kullanmayınız.

Bu betik `/home/$USER/` dizininize `works/lando` şeklinde bir dizin açar ve dolayısıyla Drupal kurulumlarınızı `/home/$USER/works/lando` dizininde bulabilirsiniz.

## Kurulum sonrası

Drupal kurulumunuzda admin kullanıcı ve parola adı `admin` ve `admin` olarak düzenlenmiştir.

## Notlar

>İnternet hızınıza bağlı olarak maksimum 4-5 dakika sonra kurulu bir Drupal 8 ile başbaşa olacaksınız.
>
>Makinanızda Lando'yu ilk kez çalıştırıyorsanız, `docker` imajlarını ilk defa indireceğinden süre biraz daha uzayabilir. Ancak ikinci ve sonrasındaki çalıştırmalarda imaj indirilmeyeceğinden çok hızlı kurulum olacaktır.

Klasik kurulum betiği için [tıklayınız](https://gitlab.com/snippets/1795409).