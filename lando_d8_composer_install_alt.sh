#!/bin/bash

# Kullanım
# Dikkat! Script parametresinde Türkçe karakter
# ve boşluk kullanmayınız.
# parametre 1 = dizin
# bash lando_d8_composer_install_alt.sh "test-lando-script"
# bash $HOME/scripts/lando_d8_composer_install_alt.sh "test-lando-script"

# Functions begin
function displaytime {
  local T=$1
  local D=$((T/60/60/24))
  local H=$((T/60/60%24))
  local M=$((T/60%60))
  local S=$((T%60))
  (( $D > 0 )) && printf '%d days ' $D
  (( $H > 0 )) && printf '%d hours ' $H
  (( $M > 0 )) && printf '%d minutes ' $M
  (( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
  printf '%d seconds\n' $S
}
# Functions end

START=$(date +%s)

if [ ! -z "$1" ]; then
  clear
  
  echo "Installation begins ->" $(date)
  
  LANDOPATH=/home/$USER/works/lando
  
  APP_DIRECTORY=$LANDOPATH/$1
  if [ -d "$APP_DIRECTORY" ]; then
    # Control will enter here if $DIRECTORY exists.
    clear
    echo "******* VERDİĞİNİZ PARAMETRENİN ADINI TAŞIYAN BİR DİZİN VAR *******"
    exit 1
  else
    mkdir -p $APP_DIRECTORY
    cd $APP_DIRECTORY
    
    echo ".gitignore file is being generated..."
    wget -q https://gitlab.com/binbiriz/lando-recipes-extended/raw/master/templates/template.gitignore -O .gitignore
    echo "README.md file is being generated..."
    touch README.md > /dev/null
    echo "Git repository is being initialized..."
    git init > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "initial commit - lando first config" > /dev/null
    
    echo "Lando configuration file is being generated..."
    wget -q https://gitlab.com/binbiriz/lando-recipes-extended/raw/master/landoymls/template-d8-xdebug-lando.yml -O .lando.yml
    # add app name to the beginning of file
    sed -i '1i\\' .lando.yml
    { echo -n "name: $1-$(date +%s)"; cat .lando.yml; } >.lando.yml.new
    mv .lando.yml{.new,}
    
    echo "xDebugging for Visual Studio Code configuration is being generated..."
    mkdir .vscode
    touch ./.vscode/xdebug.log  > /dev/null
    
    wget -q https://gitlab.com/binbiriz/lando-recipes-extended/raw/master/vscode-templates/launch.json -O ./.vscode/launch.json
    wget -q https://gitlab.com/binbiriz/lando-recipes-extended/raw/master/vscode-templates/php.ini -O ./.vscode/php.ini
    
    # exit
    
    echo "Downloading Drupal 8 with composer..."
    # There is no need for the composer process to be shown and this is only achived with '> /dev/null 2>&1'
    composer create-project drupal-composer/drupal-project:8.x-dev drupal --stability dev --no-interaction > /dev/null 2>&1
    
    cp ./drupal/web/sites/example.settings.local.php ./drupal/web/sites/default/settings.local.php
    cp ./drupal/web/sites/default/default.services.yml ./drupal/web/sites/default/services.yml
    
    echo "" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "if (file_exists(\$app_root . '/' . \$site_path . '/settings.local.php')) {" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "  include \$app_root . '/' . \$site_path . '/settings.local.php';" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "}" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    
    echo "" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "\$settings['trusted_host_patterns'] = [" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "  '^lndo\.site$'," | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "  '^.+\.lndo\.site$'," | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "  '^localhost$'," | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    echo "];" | tee --append ./drupal/web/sites/default/settings.php > /dev/null
    
    sed -i '7i\  twig.config:\' ./drupal/web/sites/development.services.yml
    sed -i '8i\    debug: true\' ./drupal/web/sites/development.services.yml
    sed -i '9i\    auto_reload: null\' ./drupal/web/sites/development.services.yml
    sed -i '10i\    cache: false\' ./drupal/web/sites/development.services.yml
    
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "initial Drupal 8 downloaded with composer" > /dev/null
    
    # DİKKAT BURADA LANDO BAŞLATILACAK
    # echo "*****************************************"
    # echo "*****************************************"
    # echo "DİKKAT 2 SANİYE SONRA LANDO BAŞLATILACAK"
    # echo "*****************************************"
    # echo "*****************************************"
    # echo "emin değilseniz CTRL+C basarak iptal ediniz"
    # read -n1 -s -r -p "Press any key to continue"
    
    echo "Lando is starting..."
    sleep 2s
    
    lando start
    
    # DİKKAT BURADA DRUSH İLE SİTE KURULACAK
    # echo "*****************************************************"
    # echo "*****************************************************"
    # echo "DİKKAT BURADA 2 SANİYE SONRA DRUSH İLE SİTE KURULACAK"
    # echo "*****************************************************"
    # echo "*****************************************************"
    # echo "emin değilseniz CTRL+C basarak iptal ediniz"
    # read -n1 -s -r -p "Press any key to continue"
    
    echo "Drupal site is being installed with drush site-install..."
    sleep 2s
    
    lando drush site-install standard --locale=en --db-url=mysql://drupal8:drupal8@database:3306/drupal8 --site-name=Drupal_8_via_Composer_With_Lando --account-name=admin --account-pass=admin --yes > /dev/null
    
    echo "Exporting configuration via drush cex"
    lando drush cex --yes > /dev/null
    
    echo "Git add files..."
    git add . > /dev/null
    
    echo "Git commit..."
    git commit -m "first configuration export" > /dev/null
    
    # lando info
    
    echo "Installing module filter module"
    lando composer require drupal/module_filter --no-interaction > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Install drupal/module_filter" > /dev/null
    echo "Enabling drupal/module_filter"
    lando drush en module_filter --yes > /dev/null
    echo "Exporting configuration via drush cex"
    lando drush cex --yes > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Enable drupal/module_filter" > /dev/null
    
    echo "Installing admin_toolbar and toolbar_anti_flicker"
    lando composer require 'drupal/admin_toolbar:^2.0' 'drupal/toolbar_anti_flicker:^3.2' --no-interaction > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Install drupal/admin_toolbar and drupal/toolbar_anti_flicker" > /dev/null
    echo "Enabling drupal/admin_toolbar and drupal/toolbar_anti_flicker"
    lando drush en admin_toolbar toolbar_anti_flicker --yes > /dev/null
    echo "Exporting configuration via drush cex"
    lando drush cex --yes > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Enable drupal/admin_toolbar and drupal/toolbar_anti_flicker" > /dev/null
    
    echo "Installing coffee"
    lando composer require 'drupal/coffee' --no-interaction > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Install drupal/coffee" > /dev/null
    echo "Enabling drupal/coffee"
    lando drush en coffee --yes > /dev/null
    echo "Exporting configuration via drush cex"
    lando drush cex --yes > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Enable drupal/coffee" > /dev/null
    
    echo "Installing devel"
    lando composer require 'drupal/devel' --no-interaction > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Install drupal/devel" > /dev/null
    echo "Enabling devel and devel generate"
    lando drush en devel devel_generate --yes > /dev/null
    echo "Exporting configuration via drush cex"
    lando drush cex --yes > /dev/null
    echo "Git add files..."
    git add . > /dev/null
    echo "Git commit..."
    git commit -m "Enable devel and devel generate" > /dev/null
    
    lando drush cr > /dev/null
    lando drush cron > /dev/null
    
    echo "Installation ends ->" $(date)
  fi
else
  clear
  echo "******* EKSİK PARAMETRE GİRDİNİZ *******"
  echo "[lando_app_name] parametresinde kesinlikle boşluk (whitespace) kullanmayınız!"
  echo "bash lando_d8_composer_install.sh \"lando_app_name\" şeklinde kullanmalısınız!"
  echo "******* EKSİK PARAMETRE GİRDİNİZ *******"
  exit 1
fi

END=$(date +%s)
DIFF=$(($END-$START))
echo "DURATION: " $(displaytime $DIFF)