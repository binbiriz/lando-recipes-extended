# Changelog

## 12 October 2019

1. Add alternate composer installer (PHP 7.2, xDebug for Visual Code)

## 24 Şubat 2019

1. Lando kurulum linki düzeltildi.
2. Bazı tipolar düzeltildi.
3. Script'i download eden komut çoklu satır haline getirildi.