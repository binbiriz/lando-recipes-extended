#!/bin/bash

# Kullanım
# Dikkat! Script parametresinde Türkçe karakter
# ve boşluk kullanmayınız.
# parametre 1 = dizin
# bash lando_d8_classic_install.sh "test-lando-script"

# Functions begin
function displaytime {
    local T=$1
    local D=$((T/60/60/24))
    local H=$((T/60/60%24))
    local M=$((T/60%60))
    local S=$((T%60))
    (( $D > 0 )) && printf '%d days ' $D
    (( $H > 0 )) && printf '%d hours ' $H
    (( $M > 0 )) && printf '%d minutes ' $M
    (( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
    printf '%d seconds\n' $S
}
# Functions end

START=$(date +%s)

if [ ! -z "$1" ]; then
    clear
    
    echo "Installation begins ->" $(date)
    
    LANDOPATH=/home/$USER/works/lando
    
    TEMPDIR=$(mktemp -d)
    
    APP_DIRECTORY=$LANDOPATH/$1
    if [ -d "$APP_DIRECTORY" ]; then
        # Control will enter here if $DIRECTORY exists.
        clear
        echo "******* VERDİĞİNİZ PARAMETRENİN ADINI TAŞIYAN BİR DİZİN VAR *******"
        exit 1
    else
        mkdir -p $APP_DIRECTORY
        cd $APP_DIRECTORY
        echo "Lando configuration file is being generated..."
        touch .lando.yml > /dev/null
        echo "name: $1-$(date +%s)" | tee --append .lando.yml > /dev/null
        echo "recipe: drupal8" | tee --append .lando.yml > /dev/null
        echo "config:" | tee --append .lando.yml > /dev/null
        echo "  webroot: drupal/web" | tee --append .lando.yml > /dev/null
        echo "  php: '7.2'" | tee --append .lando.yml > /dev/null
        echo "  drush: global:8.2.3" | tee --append .lando.yml > /dev/null
        echo "services:" | tee --append .lando.yml > /dev/null
        echo "  mailhog:" | tee --append .lando.yml > /dev/null
        echo "    type: mailhog" | tee --append .lando.yml > /dev/null
        echo "    hogfrom:" | tee --append .lando.yml > /dev/null
        echo "      - appserver" | tee --append .lando.yml > /dev/null
        echo "  pma:" | tee --append .lando.yml > /dev/null
        echo "    type: phpmyadmin" | tee --append .lando.yml > /dev/null
        echo "tooling:" | tee --append .lando.yml > /dev/null
        echo "  drush:" | tee --append .lando.yml > /dev/null
        echo "    service: appserver" | tee --append .lando.yml > /dev/null
        echo "    cmd:" | tee --append .lando.yml > /dev/null
        echo "      - \"drush --root=/app/drupal/web\"" | tee --append .lando.yml > /dev/null
        
        echo ".gitignore file is being generated..."
        touch .gitignore > /dev/null
        echo "README.md file is being generated..."
        touch README.md > /dev/null
        echo "Git repository is being initialized..."
        git init > /dev/null
        echo "Git add files..."
        git add . > /dev/null
        echo "Git commit..."
        git commit -m "initial commit - lando first config" > /dev/null
        
        echo "Downloading Drupal 8 with wget..."
        wget -O $TEMPDIR/drupal8-core-latest-stable.zip https://www.drupal.org/download-latest/zip > /dev/null 2>&1
        
        echo "Unzipping downloaded Drupal 8..."
        unzip $TEMPDIR/drupal8-core-latest-stable.zip -d $TEMPDIR/d8-latest > /dev/null
        
        DRUPALLATESTDIR=$(find $TEMPDIR/d8-latest/ -maxdepth 1 -type d -iname 'drupal*' )
        
        echo "Moving extracted Drupal 8 to the proper folder..."
        mkdir -p $APP_DIRECTORY/drupal > /dev/null
        mv $DRUPALLATESTDIR/ $APP_DIRECTORY/drupal/web/ > /dev/null
        rm -rf $TEMPDIR/ > /dev/null
        
        echo "Generating proper .gitignore file for Drupal 8..."
        touch $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "# Ignore configuration files that may contain sensitive information." | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "sites/*/settings*.php" | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "sites/*/services*.yml" | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "# Ignore paths that contain user-generated content." | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "sites/*/files" | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "sites/*/private" | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "# Ignore SimpleTest multi-site environment." | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        echo "sites/simpletest" | tee --append $APP_DIRECTORY/drupal/web/.gitignore > /dev/null
        
        echo "Git add files..."
        git add . > /dev/null
        echo "Git commit..."
        git commit -m "initial Drupal 8 downloaded via classic way" > /dev/null
        
        # DİKKAT BURADA LANDO BAŞLATILACAK
        # echo "*****************************************"
        # echo "*****************************************"
        # echo "DİKKAT BURADA 5 SANİYE LANDO BAŞLATILACAK"
        # echo "*****************************************"
        # echo "*****************************************"
        # echo "emin değilseniz CTRL+C basarak iptal ediniz"
        # read -n1 -s -r -p "Press any key to continue"
        
        echo "Lando is starting..."
        sleep 5s
        
        lando start
        
        # DİKKAT BURADA DRUSH İLE SİTE KURULACAK
        # echo "*****************************************************"
        # echo "*****************************************************"
        # echo "DİKKAT BURADA 5 SANİYE SONRA DRUSH İLE SİTE KURULACAK"
        # echo "*****************************************************"
        # echo "*****************************************************"
        # echo "emin değilseniz CTRL+C basarak iptal ediniz"
        # read -n1 -s -r -p "Press any key to continue"
        
        echo "Drupal site is being installed with drush site-install..."
        sleep 5s
        
        lando drush site-install standard --locale=en --db-url=mysql://drupal8:drupal8@database:3306/drupal8 --site-name=Drupal_8_Classic_With_Lando --account-name=admin --account-pass=admin --yes > /dev/null
        
        echo "Moving config folder to a proper place..."
        # mkdir -p $APP_DIRECTORY/drupal/web/config
        
        echo "\$config_directories['sync'] = '../config/sync';" | sudo tee --append $APP_DIRECTORY/drupal/web/sites/default/settings.php  > /dev/null
        
        CONFIGDIR=$(find $APP_DIRECTORY/drupal/web/sites/default/files/ -maxdepth 1 -type d -iname 'config_*' )
        
        #find ./web/sites/default/files/ -maxdepth 1 -type d -iname "config_*"
        
        # mv ./sites/default/files/config_EfIkUA5mK6nP5fU1Jtl2Uw--cY0A1mxEdPXr8F6Ad8yYLUCRC_GsYJdtVwjBE2OKkpNS3F_w9g ../config
        
        mv $CONFIGDIR $APP_DIRECTORY/drupal/config > /dev/null
        
        echo "Exporting configuration via drush cex"
        lando drush cex --yes > /dev/null
        
        echo "Git add files..."
        git add . > /dev/null
        
        echo "Git commit..."
        git commit -m "first configuration export" > /dev/null
        
        # lando info
        
        echo "Installation ends ->" $(date)
    fi
else
    clear
    echo "******* EKSİK PARAMETRE GİRDİNİZ *******"
    echo "[lando_app_name] parametresinde kesinlikle boşluk (whitespace) kullanmayınız!"
    echo "bash lando_d8_classic_install.sh \"lando_app_name\" şeklinde kullanmalısınız!"
    echo "******* EKSİK PARAMETRE GİRDİNİZ *******"
    exit 1
fi

END=$(date +%s)
DIFF=$(($END-$START))
echo "DURATION: " $(displaytime $DIFF)